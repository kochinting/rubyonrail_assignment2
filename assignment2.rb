
class Assignment2

  file=File.open("assignment_two_text.txt", "rb")
  contents= file.read
  file.close

  words = contents.split (/\s+/)
  word_frequency= Hash.new(0)
  words.each {|word| word_frequency [word.split("@").last.gsub(/\W\z/, '').chomp(")")]+=1 if word=~/.*@\w*/}

  Hash[word_frequency.sort].each_pair do |key, value|
    puts "#{key.rjust(12)}: #{value} time(s)"
  end

 end
